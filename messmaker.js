export const version = "3.1.0-beta";

export class Mess {
	constructor(container) {
		if (!(container instanceof HTMLElement)) {
			throw new TypeError("container is not an HTML element");
		}
		
		this.container = container;
	}
	
	run() {
		this.fuckness = requestAnimationFrame(() => this.fuckEverythingUp());
	}
	pause() {
		cancelAnimationFrame(this.fuckness);
	}
	
	fuckEverythingUp() {
		this.run();
		
		this.container.style.background =
		
		[this.container].concat(Array.from(
			this.container.querySelectorAll("*")
		)).forEach(function(currentElement) {
			currentElement.style.display = "block";
			
			currentElement.style.transition = "all 0.166s";
			
			currentElement.style.background =
`linear-gradient(
	${[
		Math.random() * 180,
		Math.random() * -180
	][Math.floor(Math.random() * 2)]}deg,
	#${Math.floor(Math.random() * 2 ** 32).toString(16)},
	#${Math.floor(Math.random() * 2 ** 32).toString(16)}
)`
			
			currentElement.style.fontFamily = [
				"sans-serif",
				"serif",
				"monospace",
				"Courier New",
				"fantasy",
				"cursive",
				"Comic Sans MS",
				"Lobster",
				"Times New Roman",
				"Zilla Slab"
			][Math.floor(Math.random() * 9)];
			currentElement.style.fontSize = Math.random() * 64 + "px";
			currentElement.style.fontWeight = Math.floor(
				Math.random() * 10
			) * 100;
			currentElement.style.fontVariant = [
				"normal",
				"small-caps"
			][Math.floor(Math.random() * 2)];
			currentElement.style.fontStyle = [
				"normal",
				"italic"
			][Math.floor(Math.random() * 2)];
			currentElement.style.textTransform = [
				"capitalize",
				"uppercase",
				"lowercase",
				"none",
				"full-width"
			][Math.floor(Math.random() * 5)];
			currentElement.style.textDecoration = [
				"underline",
				"overline",
				"wavy underline overline",
				"dotted underline overline",
				"none",
				"line-through"
			][Math.floor(Math.random() * 6)];
			currentElement.style.color =
`#${Math.floor(Math.random() * 2 ** 32).toString(16)}`;
			
			currentElement.style.border =
`${Math.random() * 16}px
${[
	"hidden",
	"dotted",
	"dashed",
	"solid",
	"double",
	"groove",
	"ridge",
	"inset",
	"outset"
][Math.floor(Math.random() * 9)]}
#${Math.floor(Math.random() * 2 ** 32).toString(16)}`;
			currentElement.style.borderRadius =
`${Math.random() * 64}%
${Math.random() * 64}%
${Math.random() * 64}%
${Math.random() * 64}%`;
			
			currentElement.style.transform =
`scale(
	${0.5 + Math.random()},
	${0.5 + Math.random()}
)
rotate(${[
	Math.random() * 5,
	Math.random() * -5
][Math.floor(Math.random() * 2)]}deg)
translate(
	${[
		Math.random() * 25,
		Math.random() * -25
	][Math.floor(Math.random() * 2)]}%,
	${[
		Math.random() * 25,
		Math.random() * -25
	][Math.floor(Math.random() * 2)]}%
)
skew(
	${[
		Math.random() * 5,
		Math.random() * -5
	][Math.floor(Math.random() * 2)]}deg,
	${[
		Math.random() * 5,
		Math.random() * -5
	][Math.floor(Math.random() * 2)]}deg
)`;
		});
	}
}
